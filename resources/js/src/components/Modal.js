<template>
  <modal name="example"
         :width="300"
         :height="300"
         :adaptive="true">
    Hello, Properties!
  </modal>
</template>
<script>
export default {
  name: 'ExampleModal'
}
</script>