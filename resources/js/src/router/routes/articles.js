export default [
  {
    path: '/articles/create',
    name: 'articles-create',
    component: () => import('@/views/articles/Create.vue'),
  },
]